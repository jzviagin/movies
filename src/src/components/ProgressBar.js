import {Text, StyleSheet} from 'react-native'
import React from 'react'

import {SafeAreaView} from 'react-navigation'


export default (props)=>{
    return (
        <SafeAreaView style = {styles.spinnerTextContainer} forceInset= {{top: 'always'}}>
            <Text style = {styles.spinnerText} value="Please wait...">Please wait...</Text>
        </SafeAreaView>
   
    )
}

const styles = StyleSheet.create({
    spinnerText:{
        fontSize:40
    },
    spinnerTextContainer:{
        display: "flex",
        flex: 1,
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    main:{
        display: 'flex',
        flex: 1
    }
});