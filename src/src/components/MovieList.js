import React, {useEffect, useContext} from 'react';
import {View, StyleSheet, Text, Button, FlatList, TouchableOpacity} from 'react-native';
import {ListItem} from 'react-native-elements'
import {Context as MoviesContext}  from '../context/movieContext'
//import {NavigationEvents, withNavigationFocus} from 'react-navigation'
import { FontAwesome } from '@expo/vector-icons'; 
import {connect} from 'react-redux'
import { withNavigation} from 'react-navigation'


const MovieList = (props) => {
    const {state, fetchMovies} = useContext(MoviesContext);
    console.log(state)
    console.log('movie list');
         /*   {props.favs<NavigationEvents onWillFocus = {() => { 
             fetchMovies();
        }} />
        }*/
    useEffect( ()=>{
        fetchMovies();
    },[])

    let movies = state.movies;

    if (props.favorites){
        movies = movies.filter( (movie)=>{
            if(props.favMovies.includes(movie.id)){
                return true;
            }
            return false;
        })
    }

    return (
        <>


        
        
        <FlatList data = {movies}
            keyExtractor = {item => item.id}
            renderItem = {
                (item) => {                    
                    return (<TouchableOpacity style= {styles.itemContainer} onPress = { ()=> {
                        props.navigation.navigate('MovieDetails', {id: item.item.id});
                        if (props.onMovieSelected){
                            props.onMovieSelected();
                        }}}>
                        
                        <View style= {styles.chevron}>
                            <FontAwesome  name="chevron-circle-right" size={34} color="black" />
                        </View>
                        <View style= {styles.itemContainerData}>
                            <Text  numberOfLines={1} ellipsizeMode='tail' style= {styles.itemTitle} >{item.item.title}</Text>
                        </View>

                    </TouchableOpacity>
                    )
                }
            }>
        
        </FlatList>


        
        </>
        )
};





const styles = StyleSheet.create({
    itemContainerData: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: "flex-start",
        margin:1,
        flex:1,
        backgroundColor: 'white'
    },
    itemContainer: {
        display: 'flex',
        flexDirection: 'row-reverse',
        alignItems: "center",
        margin:1,
        backgroundColor: 'white'
    },
    itemTitle: {
        padding: 10,
        fontSize:22,
        fontWeight:'bold'
    },
    itemTime: {
        padding: 10
    },
    chevron:{
        display: "flex",
        flex: 1,
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginEnd: 15
        
    }
});

const mapStateToProps = (state) =>{
    return {
        favMovies:state.movies
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        //onFetchOrders: (token, userId) => dispatch(locationActions.fetchOrdersStart (token, userId)),
        //onAddIngredient: (ingredient)=> dispatch({type:actionTypes.ADD_INGREDIENT, ingredient: ingredient}),
       // onRemoveIngredient: (ingredient)=> dispatch({type:actionTypes.REMOVE_INGREDIENT, ingredient: ingredient})

        addMovie: (movie)=>dispatch(favoriteActions.addMovie(movie)),
        removeMovie: (movie)=>dispatch(favoriteActions.removeMovie(movie))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(MovieList));

