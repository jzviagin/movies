

export const addMovie =  (movie)=>{
    return {
            type: 'add_movie',
            movie: movie
        }
    
}
export const removeMovie =  (movie)=>{
    return {
            type: 'remove_movie',
            movie: movie
        }
    
}

