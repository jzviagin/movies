
export default  favoriteReducer = (state = { movies : [] }, action) => {
    switch(action.type){

        case 'add_movie':
            return {...state, movies: [...state.movies, action.movie]}
        case 'remove_movie':
            return {...state, movies: state.movies.filter((item)=> {
                if (action.movie === item){
                    return false;
                }
                return true;
            })}
        default:
            return state;
    }
}

