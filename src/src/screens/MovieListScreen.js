import React, {useEffect, useContext} from 'react';
import {View, StyleSheet, Text, Button, FlatList, TouchableOpacity} from 'react-native';
import {ListItem} from 'react-native-elements'
import {Context as MoviesContext}  from '../context/movieContext'
import {NavigationEvents} from 'react-navigation'
import { FontAwesome } from '@expo/vector-icons'; 

import MovieList from "../components/MovieList"
/**
 * 
 * <FlatList data = {state.movies}
            keyExtractor = {item => item.id}
            renderItem = {
                (item) => {                    
                    return (<TouchableOpacity style= {styles.itemContainer} onPress = { ()=> {props.navigation.navigate('MovieDetails', {id: item.item.id})}}>
                        
                        <View style= {styles.chevron}>
                            <FontAwesome  name="chevron-circle-right" size={34} color="black" />
                        </View>
                        <View style= {styles.itemContainerData}>
                            <Text  numberOfLines={1} ellipsizeMode='tail' style= {styles.itemTitle} >{item.item.title}</Text>
                        </View>

                    </TouchableOpacity>
                    )
                }
            }>
        
        </FlatList>
 */

const MovieListScreen = (props) => {
    //const {state, fetchMovies} = useContext(MoviesContext);





    return (
        <>
        <NavigationEvents onWillFocus = {() => { 
            // fetchMovies();
        }} />
        <MovieList/>
        
        
        


        
        </>
        )
};

MovieListScreen.navigationOptions = {
    title: 'Movies'
}



const styles = StyleSheet.create({
    itemContainerData: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: "flex-start",
        margin:1,
        flex:1,
        backgroundColor: 'white'
    },
    itemContainer: {
        display: 'flex',
        flexDirection: 'row-reverse',
        alignItems: "center",
        margin:1,
        backgroundColor: 'white'
    },
    itemTitle: {
        padding: 10,
        fontSize:22,
        fontWeight:'bold'
    },
    itemTime: {
        padding: 10
    },
    chevron:{
        display: "flex",
        flex: 1,
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginEnd: 15
        
    }
});

export default MovieListScreen;