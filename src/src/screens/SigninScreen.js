import React, { useContext, useState} from 'react';
import {View, StyleSheet, TouchableOpacity, ActivityIndicator, Image} from 'react-native';
import {NavigationEvents, SafeAreaView} from 'react-navigation'
import {Text, Button} from 'react-native-elements'
//import img from '../../assets/user.png'

import useAuth from '../hooks/useAuth'

import {Context as AuthContext} from '../context/authContext'
import UserDetails from "../components/UserDetails"




const SigninScreen = ({navigation}) => {


    console.log('rendering sign in')

    const [LoginComponent,logOut] = useAuth();

    const {state, setUser,setLoading} = useContext(AuthContext);

    if(state.user){
      navigation.navigate('mainFlow');
    }

    /**
     * 
     *  < Text style = {styles.welcome}>Welcome stranger</Text>
          <View style={styles.imageContainer}>
          <Image
              style={styles.image}
          
              PlaceholderContent={<ActivityIndicator />}
              />
          </View>
      * 
      */
  


    return (
        <SafeAreaView style = {styles.mainContainer} forceInset= {{top: 'always'}}>
            <NavigationEvents onWillFocus = { ()=>{
              
            }}/>

            <View style = {styles.container}>
              <UserDetails userName = "stranger" photo = {null}/>
             
              < Text style = {styles.welcome}>Please ign in to see the movies</Text>
              
                
            
          </View>

            <LoginComponent/>
            {state.user?
            <Image
            source={{ uri: 'user.png' }}
            style={{  width: '100%', height:300 }}
            PlaceholderContent={<ActivityIndicator />}
            />:null}




           
        </SafeAreaView>
        
    )

    
};


SigninScreen.navigationOptions =  {
    title:'Home',
    header: null,
    headerShown: false
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'column',
        marginBottom: 0
    },
    mainContainer: {
      flex: 1,
      justifyContent:'center',
      alignItems: 'center',
      flexDirection: 'column',
      marginBottom: 0,

  },
  welcome: {
    fontSize:20, margin:20
},
   
    imageContainer: {
      marginTop: 20,
      marginBottom: 20,
      borderRadius:150,
      width: 300, height:300 ,

      borderWidth: 2,
      borderColor: "grey",
      overflow: 'hidden'
  },
  image: {
      width: '100%' , height:'100%' 
   
  }
});

export default SigninScreen;