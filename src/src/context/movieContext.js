import createDataContext from './createDataContext'



import movieApi from '../api/movie'

const movieReducer = (state, action) => {
    switch(action.type){
        case 'fetch_movies':
            return {...state, movies: action.movies}
        default:
            return state;
    }
}







const fetchMovies = dispatch => async ()=>{

    try{
        const response = await movieApi.get('/movie/popular');
        console.log('res', response);
          dispatch ({
            type: 'fetch_movies',
            movies:response.data.results
         });
        
    }catch(error){
        console.log(error);
        
    }
    
}




export const {Provider, Context} = createDataContext(movieReducer, { fetchMovies}, {movies:[]})